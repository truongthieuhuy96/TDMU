import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { loginService } from '../service/login.service';
import { HeaderComponent } from '../template/header/header.component';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.css'],
})
export class LogOutComponent implements OnInit {

    constructor(private router: Router, private _login: loginService) {
        localStorage.removeItem('x-user');
        HeaderComponent.updateUserStatus.next(); // here!
        this.router.navigate(['/']);
    }

    ngOnInit() {
    }

}
