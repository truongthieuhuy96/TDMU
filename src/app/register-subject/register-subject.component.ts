import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../template/header/header.component';

@Component({
  selector: 'app-register-subject',
  templateUrl: './register-subject.component.html',
  styleUrls: ['./register-subject.component.css']
})
export class RegisterSubjectComponent implements OnInit {

  constructor() { 
    HeaderComponent.updateUserStatus.next();
  }
  

  ngOnInit() {
  }

}
