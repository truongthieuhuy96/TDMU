import { IMonhoc } from './../model/monhoc.model';
import { ISinhvien } from './../model/sinhvien.model';
import { Component, OnInit } from '@angular/core';
import { sinhvienService } from '../service/sinhvien.service';
import { monhocService } from '../service/monhoc.service';
import { predictService } from '../service/predict.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-predict',
  templateUrl: './predict.component.html',
  styleUrls: ['./predict.component.scss']
})
export class PredictComponent implements OnInit {
  content;
  p: Number = 1;
  monhocs: any;
  dudoans: any[];
  data: any[];
  result: any[];
  evaluate: any;
  req = {
    algorithm: "",
    model: ""
  }
  test = 0;

  constructor(private _predictService: predictService) {

    // this._predictService.getResultData(this.req)
    // this._predictService.getResult.subscribe(data => {
    //   this.result = data
    // });

    // this._predictService.getDataEvaluate(this.req);
    // this._predictService.getEvaluate.subscribe(data => {
    //   this.evaluate = data;
    // });

    // this._predictService.getDataDuDoan(this.req);
    // this._predictService.getDuDoan.subscribe(data => {
    //   this.dudoans = data;
    // });

    // this._predictService.getDataModel(this.req);
    // this._predictService.getData.subscribe(data => {
    //   this.data = data
    // });

    // this._monhocService.getDataMonHoc();
    // this._monhocService.getMonHoc.subscribe(data => {
    //   this.monhocs = data;
    // });

  }

  ngOnInit() {

  }

  getContent() {
    this.content = 0;
    this.test += 1;
    this._predictService.setRequest(this.req);
  }

  getRecomment() {
    this.content = 2;
    this.test += 1;
    this._predictService.setRequest(this.req);
  }

  getNewData() {
    this.content = 1;
    this._predictService.getResultData(this.req)
    this._predictService.getResult.subscribe(data => {
      this.result = data
    });

    this._predictService.getDataEvaluate(this.req);
    this._predictService.getEvaluate.subscribe(data => {
      this.evaluate = data;
    });

    this._predictService.getDataDuDoan(this.req);
    this._predictService.getDuDoan.subscribe(data => {
      this.dudoans = data;
    });

    this._predictService.getDataModel(this.req);
    this._predictService.getData.subscribe(data => {
      this.data = data
    });

    this.monhocs = Array(52).fill(1);
  }

}
