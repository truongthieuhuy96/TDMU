import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './template/home/home.component';
import { RegisterSubjectComponent } from './register-subject/register-subject.component';
import { PredictComponent } from './predict/predict.component';
import { LogOutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'dangnhap', component: LoginComponent },
  { path: 'dangkimonhoc', component: RegisterSubjectComponent },
  { path: 'dudoan', component: PredictComponent},
  { path: 'dangxuat', component: LogOutComponent},
  { path: 'thongtinsinhvien', component: ProfileComponent},
  { path: 'test', component: TestComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RoutingModule {}
