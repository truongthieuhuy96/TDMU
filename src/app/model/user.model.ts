export interface IUser{
    username: String;
    avatar: String;
    token: String;
}