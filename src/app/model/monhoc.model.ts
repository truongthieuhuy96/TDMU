export interface IMonhoc {
    _id?: any;
    idmonhoc: Number;
    mamonhoc: String;
    tenmonhoc: String;
    monhoctienquyet: String;
    sotinchi: String;
}