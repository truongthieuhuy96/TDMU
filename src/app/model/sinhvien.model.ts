export interface ISinhvien{
    _id?: any;
    tensinhvien: String;
    masinhvien: String;
    lop: String;
    khoa: String;
    nganh: String;
    idsinhvien: Number;
}