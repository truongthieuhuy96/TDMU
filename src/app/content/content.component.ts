import { IMonhoc } from './../model/monhoc.model';
import { Component, OnInit, Input } from '@angular/core';
import { sinhvienService } from '../service/sinhvien.service';
import { monhocService } from '../service/monhoc.service';
import { predictService } from '../service/predict.service';

@Component({
    selector: 'content',
    templateUrl: 'content.component.html',
    styleUrls: ['content.component.scss']
})

export class ContentComponent implements OnInit {
    request = {
        model: 0
      }
    p: Number = 1;
    monhocs: IMonhoc[];
    dudoans: any[];
    data: any[];
    result: any[];
    evaluate: any;

    constructor(private _sinhvienService: sinhvienService, private _monhocService: monhocService, private _predictService: predictService) {
        this._predictService.getResultData(this.request)
        this._predictService.getResult.subscribe(data => {
            this.result = data
        });

        this._predictService.getDataEvaluate(this.request);
        this._predictService.getEvaluate.subscribe(data => {
            this.evaluate = data;
        });

        this._predictService.getDataDuDoan(this.request);
        this._predictService.getDuDoan.subscribe(data => {
            this.dudoans = data;
        });

        this._predictService.getDataModel(this.request);
        this._predictService.getData.subscribe(data => {
            this.data = data
        });

        this._monhocService.getDataMonHoc();
        this._monhocService.getMonHoc.subscribe(data => {
            this.monhocs = data;
        });
    }

    ngOnInit() { }
}