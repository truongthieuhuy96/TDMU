import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Chart } from 'chart.js';

@Injectable()
export class ChartService {
    private _chart: Subject<any> = new Subject();
    get getChart() {
        return this._chart.asObservable();
    }

    buildChart(ctx, data) {
        this._chart.next(new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Traning", "Test"],
                datasets: [{
                    label: '# of Votes',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: false,
                display: true
            }
        }));
    }
    constructor() { }
}