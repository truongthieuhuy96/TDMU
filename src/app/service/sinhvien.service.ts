import { Http, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { ISinhvien } from '../model/sinhvien.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class sinhvienService{
    private _sinhvien: Subject<ISinhvien> = new Subject();
    constructor(private Http: Http){
    }
    get getSinhVien(){
        return this._sinhvien.asObservable();
    }
    
    getDataSinhvien(token): Promise<any>{
        const headers = new Headers({'x-access-token': token});
        const options = new RequestOptions({
            headers: headers
          });
        return this.Http.get('http://localhost:8080/api/sinhvien', {headers:headers})
        .toPromise()
        .then(data => {
            this._sinhvien.next(data.json());
            return data.json();
        })
        .catch(err => {
            return err;
        });
    }
}