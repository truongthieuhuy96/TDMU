import { Http, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { IUser } from '../model/user.model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class loginService{
    private _user: Subject<IUser> = new Subject();
    constructor(private Http: Http){
    }
    get getUser(){
        return this._user.asObservable();
    }

    login(user) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
          });
       return this.Http.post('http://localhost:8080/api/login', JSON.stringify(user), {headers:headers})
       .toPromise()
       .then((response) => {
           this._user.next(response.json());
           return response.json();
       })
       .catch(err => err);
    }

    logout() {
        this._user.next(null);
    }

    updateAvatar(image: File,token) {
        let formData = new FormData();
        formData.append('file', image, image.name);
        const headers = new Headers({'x-access-token': token});
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
          });
       return this.Http.post('http://localhost:8080/api/avatar', formData, {headers:headers})
       .toPromise()
       .then((response) => {
           return response.json();
       })
       .catch(err => err);
    }
}