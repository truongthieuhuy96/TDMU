import { IMonhoc } from './../model/monhoc.model';
import { Http, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class monhocService{
    private _monhocs: BehaviorSubject<Array<IMonhoc>> = new BehaviorSubject(new Array());
    constructor(private Http: Http){
    }

    get getMonHoc(){
        return this._monhocs.asObservable();
    }

    getDataMonHoc(): Promise<any>{
        return this.Http.get('http://localhost:8080/api/monhoc')
        .toPromise()
        .then(data => {
            this._monhocs.next(data.json());
            return data.json();
        })
        .catch(err => {
            return err;
        });
    }
}