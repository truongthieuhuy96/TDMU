import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class predictService {
    private _dudoan: BehaviorSubject<Array<any>> = new BehaviorSubject(new Array());
    private _data: BehaviorSubject<Array<any>> = new BehaviorSubject(new Array());
    private _result: BehaviorSubject<Array<any>> = new BehaviorSubject(new Array());
    private _evaluate: Subject<any> = new Subject();
    private _percent: Subject<any> = new Subject();
    private _full: Subject<any> = new Subject();
    public _request: any;
    constructor(private http: Http) { }

    public setRequest(data: any) {
        this._request = data;
    }

    get getPercent() {
        return this._percent.asObservable();
    }

    get getFull() {
        return this._full.asObservable();
    }

    get getEvaluate() {
        return this._evaluate.asObservable();
    }

    get getDuDoan() {
        return this._dudoan.asObservable();
    }

    get getResult() {
        return this._result.asObservable();
    }

    get getData() {
        return this._data.asObservable();
    }

    getDataFull(model, masv): Promise<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
        });
        var temp = {
            algorithm: model.algorithm,
            model: model.model,
            masv: masv
        }
        if (temp.algorithm == "2") {
            return this.http.post('http://127.0.0.1:3000/api/full', JSON.stringify(temp), { headers: headers })
                .toPromise()
                .then(data => {
                    this._full.next(data.json().data);
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "1") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cf/full', JSON.stringify(temp), { headers: headers })
                .toPromise()
                .then(data => {
                    this._full.next(data.json().data);
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "0") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cb/full', JSON.stringify(temp), { headers: headers })
                .toPromise()
                .then(data => {
                    this._full.next(data.json().data);
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
    }

    getDataPercent(model): Promise<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
        });
        var temp = {
            algorithm: model.algorithm,
            model: model.model,
            k: model.k
        }
        if (temp.algorithm == "2") {
            return this.http.post('http://127.0.0.1:3000/api/percentdata', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._evaluate.next(data.json());
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "1") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cf/percentdata', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._evaluate.next(data.json());
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "0") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cb/percentdata', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._evaluate.next(data.json());
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
    }

    getDataEvaluate(model): Promise<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
        });
        var temp = {
            algorithm: model.algorithm,
            model: model.model,
            k: model.k
        }
        if (temp.algorithm == "2") {
            return this.http.post('http://127.0.0.1:3000/api/predict/evaluate', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._evaluate.next(data.json());
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "1") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cf/evaluate', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._evaluate.next(data.json());
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "0") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cb/evaluate', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._evaluate.next(data.json());
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
    }

    getDataDuDoan(model): Promise<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
        });
        var temp = {
            algorithm: model.algorithm,
            model: model.model,
            k: model.k
        }
        if (temp.algorithm == "2") {
            return this.http.post('http://127.0.0.1:3000/api/predict/all', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._dudoan.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "1") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cf/all', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._dudoan.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "0") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cb/all', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._dudoan.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
    }

    getDataModel(model): Promise<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
        });
        var temp = {
            algorithm: model.algorithm,
            model: model.model,
            k: model.k
        }
        if (temp.algorithm == "2") {
            return this.http.post('http://127.0.0.1:3000/api/predict/data', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._data.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "1") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cf/data', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._data.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "0") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cb/data', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._data.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
    }

    getResultData(model): Promise<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({
            headers: headers,
            method: RequestMethod.Post
        });
        var temp = {
            algorithm: model.algorithm,
            model: model.model,
            k: model.k
        }
        if (temp.algorithm == "2") {
            return this.http.post('http://127.0.0.1:3000/api/predict/result', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._result.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "1") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cf/result', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._result.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
        if (temp.algorithm == "0") {
            return this.http.post('http://127.0.0.1:3000/api/predict/cb/result', JSON.stringify(model), { headers: headers })
                .toPromise()
                .then(data => {
                    this._result.next(JSON.parse(data.json().data));
                    return data.json();
                })
                .catch(err => {
                    return err;
                });
        }
    }
}