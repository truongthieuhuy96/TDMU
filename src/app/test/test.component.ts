import { ChartService } from './../service/chart.service';
import { predictService } from './../service/predict.service';
import { Component, OnChanges, Input } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-test',
  templateUrl: 'test.component.html',
  styleUrls: ['test.component.scss']
})

export class TestComponent implements OnChanges {
  @Input() test: any;
  canvas: any;
  ctx: any;
  canvas1: any;
  ctx1: any;
  evaluate = [];
  percent = [];
  data: any;
  myChart: any;
  myError: any;
  image1: any;
  image2: any;

  ngOnChanges() {
    if (this.test != 1) {
      this.data = this._predictService._request;
      this._predictService.getDataPercent(this.data).then(data => {
        this.myChart.data.datasets[0].data[0] = data.traning;
        this.myChart.data.datasets[0].data[1] = data.test;
        this.myChart.update();
      })
      this._predictService.getDataEvaluate(this.data).then(data => {
        this.myError.data.datasets[0].data[0] = data.rmse * 2;
        this.myError.data.datasets[0].data[1] = data.mae * 2;
        this.myError.data.datasets[0].data[2] = data.mse * 2;
        this.myError.update();
      })
      if (this.data.algorithm == "2") {
        if (this.data.model == "0") {
          this.image1 = "../../assets/images/MF-model-1-predict.PNG";
          this.image2 = "../../assets/images/MF-model-1-result.PNG"
        }
        if (this.data.model == "1") {
          this.image1 = "../../assets/images/MF-model-2-predict.PNG";
          this.image2 = "../../assets/images/MF-model-2-result.PNG"
        }
      }
      if (this.data.algorithm == "1") {
        if (this.data.model == "0") {
          this.image1 = "../../assets/images/CF-model-1-predict.PNG";
          this.image2 = "../../assets/images/CF-model-1-result.PNG"
        }
        if (this.data.model == "1") {
          this.image1 = "../../assets/images/CF-model-2-predict.PNG";
          this.image2 = "../../assets/images/CF-model-2-result.PNG"
        }
      }
      if (this.data.algorithm == "0") {
        if (this.data.model == "0") {
          this.image1 = "../../assets/images/CB-model-1-predict.PNG";
          this.image2 = "../../assets/images/CB-model-1-result.PNG"
        }
        if (this.data.model == "1") {
          this.image1 = "../../assets/images/CB-model-2-predict.PNG";
          this.image2 = "../../assets/images/CB-model-2-result.PNG"
        }
      }
    }
  }

  constructor(private _predictService: predictService) {
    this.data = this._predictService._request;
    this._predictService.getDataPercent(this.data).then(data => {
      this.percent.push(data.traning);
      this.percent.push(data.test);
      this.canvas = document.getElementById('myChart');
      this.ctx = this.canvas.getContext('2d');
      this.myChart = new Chart(this.ctx, {
        type: 'pie',
        data: {
          labels: ["Huấn luyện", "Kiểm tra"],
          datasets: [{
            data: this.percent,
            backgroundColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          responsive: false,
          display: true
        }
      })
    });
    this._predictService.getDataEvaluate(this.data).then(data => {
      this.evaluate.push(data.rmse * 2);
      this.evaluate.push(data.mae * 2);
      this.evaluate.push(data.mse * 2);
      this.canvas1 = document.getElementById('myError');
      this.ctx1 = this.canvas1.getContext('2d');
      this.myError = new Chart(this.ctx1, {
        type: 'bar',
        data: {
          labels: ["RMSE", "MAE", "MSE"],
          datasets: [{
            data: this.evaluate,
            backgroundColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          legend: {
            display: false
          },
          responsive: false,
          display: true,
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
    }).catch(err => {
      console.log(err);
    })
    if (this.data.algorithm == "2") {
      if (this.data.model == "0") {
        this.image1 = "../../assets/images/MF-model-1-predict.PNG";
        this.image2 = "../../assets/images/MF-model-1-result.PNG"
      }
      if (this.data.model == "1") {
        this.image1 = "../../assets/images/MF-model-2-predict.PNG";
        this.image2 = "../../assets/images/MF-model-2-result.PNG"
      }
    }
    if (this.data.algorithm == "1") {
      if (this.data.model == "0") {
        this.image1 = "../../assets/images/CF-model-1-predict.PNG";
        this.image2 = "../../assets/images/CF-model-1-result.PNG"
      }
      if (this.data.model == "1") {
        this.image1 = "../../assets/images/CF-model-2-predict.PNG";
        this.image2 = "../../assets/images/CF-model-2-result.PNG"
      }
    }
    if (this.data.algorithm == "0") {
      if (this.data.model == "0") {
        this.image1 = "../../assets/images/CB-model-1-predict.PNG";
        this.image2 = "../../assets/images/CB-model-1-result.PNG"
      }
      if (this.data.model == "1") {
        this.image1 = "../../assets/images/CB-model-2-predict.PNG";
        this.image2 = "../../assets/images/CB-model-2-result.PNG"
      }
    }
  }
}