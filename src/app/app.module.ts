import { TestComponent } from './test/test.component';
import { predictService } from './service/predict.service';
import { FooterComponent } from './template/footer/footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { HeaderComponent } from './template/header/header.component';
import { LoginComponent } from './login/login.component';
import { RoutingModule } from './/app.routing.module';
import { HomeComponent } from './template/home/home.component';
import { RegisterSubjectComponent } from './register-subject/register-subject.component';
import { PredictComponent } from './predict/predict.component';
import { loginService } from './service/login.service';
import { LogOutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { sinhvienService } from './service/sinhvien.service';
import { monhocService } from './service/monhoc.service';
import { ContentComponent } from './content/content.component';
import { ChartService } from './service/chart.service';
import { RecommendComponent } from './recommend/recommend.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterSubjectComponent,
    PredictComponent,
    LogOutComponent,
    ProfileComponent,
    TestComponent,
    ContentComponent,
    RecommendComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RoutingModule,
    NgxPaginationModule
  ],
  providers: [loginService, sinhvienService, monhocService, predictService, ChartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
