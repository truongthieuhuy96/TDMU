import { IUser } from './../../model/user.model';
import { Component } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
    private user;
    public static updateUserStatus: Subject<IUser> = new Subject();

    constructor() {
        HeaderComponent.updateUserStatus.subscribe(res => {
            this.user = JSON.parse(localStorage.getItem('x-user'));
        })
    }

}