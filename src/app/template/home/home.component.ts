import {Component} from "@angular/core";
import { Router } from "@angular/router";
import { HeaderComponent } from "../header/header.component";

@Component({
    moduleId: module.id,
    selector: 'home',
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html'
})

export class HomeComponent {
    constructor(){
        HeaderComponent.updateUserStatus.next();
    }

}