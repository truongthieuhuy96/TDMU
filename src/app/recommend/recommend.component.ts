import { Component, OnChanges, Input } from '@angular/core';
import { monhocService } from '../service/monhoc.service';
import { predictService } from '../service/predict.service';

@Component({
    selector: 'app-recommend',
    templateUrl: 'recommend.component.html',
    styleUrls: ['recommend.component.scss']
})

export class RecommendComponent implements OnChanges {
    @Input() test: any;
    monhoc: any;
    diem: any;
    data: any;
    masv: any;

    constructor(private _predictService: predictService, private _monhocService: monhocService) {
        this.data = this._predictService._request;
        this._monhocService.getDataMonHoc();
        this._monhocService.getMonHoc.subscribe(data => {
            this.monhoc = data;
        })
    }

    getData() {
        console.log(this.masv)
        this._predictService.getDataFull(this.data, this.masv);
        this._predictService.getFull.subscribe(data => {
            this.diem = data
        })
    }

    ngOnChanges() {
        if (this.test != 1) {
            this.data = this._predictService._request;
        }
    }
}