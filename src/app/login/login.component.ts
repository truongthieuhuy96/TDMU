import { HeaderComponent } from './../template/header/header.component';
import {Component} from "@angular/core";
import { Router } from "@angular/router";
import { loginService } from "../service/login.service";

@Component({
    moduleId: module.id,
    selector: 'login',
    styleUrls: ['./login.component.scss'],
    templateUrl: './login.component.html'
})

export class LoginComponent {

    constructor(private _login: loginService , private router: Router){}
    model: any = {};
    token: any = {};

    getUser(){
        this._login.login(this.model);
        this._login.getUser.subscribe((data)=>{
            this.token = data;
            localStorage.setItem("x-user",JSON.stringify(this.token));
            HeaderComponent.updateUserStatus.next(data); // here!
            this.router.navigate(['/']);
        })
    }
}