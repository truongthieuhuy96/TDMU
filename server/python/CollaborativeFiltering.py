# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 13:04:17 2018

@author: DELL
"""

import pandas as pd 
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse 
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

class CF(object):
    """docstring for CF"""
    def __init__(self, Y_data, Y_test, k, dist_func = cosine_similarity, uuCF = 1):
        self.uuCF = uuCF # user-user (1) or item-item (0) CF
        self.Y_data = Y_data if uuCF else Y_data[:, [1, 0, 2]]
        self.Y_test = Y_test
        self.k = k
        self.dist_func = dist_func
        self.Ybar_data = None
        # number of users and items. Remember to add 1 since id starts from 0
        self.n_users = 32 #int(np.max(self.Y_data[:, 0])) + 1 
        self.n_items = 52 #int(np.max(self.Y_data[:, 1])) + 1
    
    def add(self, new_data):
        """
        Update Y_data matrix when new ratings come.
        For simplicity, suppose that there is no new user or item.
        """
        self.Y_data = np.concatenate((self.Y_data, new_data), axis = 0)
    
    def normalize_Y(self):
        users = self.Y_data[:, 0] # all users - first col of the Y_data
        self.Ybar_data = self.Y_data.copy()
        self.mu = np.zeros((self.n_users,))
        for n in range(self.n_users):
            # row indices of rating done by user n
            # since indices need to be integers, we need to convert
            ids = np.where(users == n)[0].astype(np.int32)
            # indices of all ratings associated with user n
            # and the corresponding ratings 
            ratings = self.Y_data[ids, 2]
            # take mean
            m = np.mean(ratings)
            if np.isnan(m):
                m = 0 # to avoid empty array and nan value
            self.mu[n] = m
            # normalize
            self.Ybar_data[ids, 2] = ratings - self.mu[n]

        ################################################
        # form the rating matrix as a sparse matrix. Sparsity is important 
        # for both memory and computing efficiency. For example, if #user = 1M, 
        # #item = 100k, then shape of the rating matrix would be (100k, 1M), 
        # you may not have enough memory to store this. Then, instead, we store 
        # nonzeros only, and, of course, their locations.
        self.Ybar = sparse.coo_matrix((self.Ybar_data[:, 2],
            (self.Ybar_data[:, 1], self.Ybar_data[:, 0])), (self.n_items, self.n_users))
        self.Ybar = self.Ybar.tocsr()

    def similarity(self):
        self.S = self.dist_func(self.Ybar.T, self.Ybar.T)
    
        
    def refresh(self):
        """
        Normalize data and calculate similarity matrix again (after
        some few ratings added)
        """
        self.normalize_Y()
        self.similarity() 
        
    def fit(self):
        self.refresh()
        
    
    def __pred(self, u, i, normalized = 1):
        """ 
        predict the rating of user u for item i (normalized)
        if you need the un
        """
        # Step 1: find all users who rated i
        ids = np.where(self.Y_data[:, 1] == i)[0].astype(np.int32)
        # Step 2: 
        users_rated_i = (self.Y_data[ids, 0]).astype(np.int32)
        # Step 3: find similarity btw the current user and others 
        # who already rated i
        #print("Test: ", users_rated_i)
        sim = self.S[u, users_rated_i]
        #print("Sim: ", sim)
        # Step 4: find the k most similarity users
        a = np.argsort(sim)[-self.k:] 
        # and the corresponding similarity levels
        nearest_s = sim[a]
        # How did each of 'near' users rated item i
        r = self.Ybar[i, users_rated_i[a]]
        if normalized:
            # add a small number, for instance, 1e-8, to avoid dividing by 0
            return (r*nearest_s)[0]/(np.abs(nearest_s).sum() + 1e-8) + self.mu[u]

        return (r*nearest_s)[0]/(np.abs(nearest_s).sum() + 1e-8) + self.mu[u]
    
    def pred(self, u, i, normalized = 1):
        """ 
        predict the rating of user u for item i (normalized)
        if you need the un
        """
        if self.uuCF: 
            return self.__pred(u, i, normalized)
        return self.__pred(i, u, normalized)
    
    def evaluate_RMSE(self, rate_test):
        n_tests = rate_test.shape[0]
        SE = 0 # squared error
        for n in range(n_tests):
            user = self.predList['user'] == rate_test[n, 0]
            item = self.predList['item'] == rate_test[n, 1]
            pred = self.predList[user & item]
            pred = pred.iloc[0]['rating']
            SE += (pred - rate_test[n, 2])**2 
        RMSE = np.sqrt(SE/n_tests)
        return RMSE
    
    def evaluate_MAE(self, rate_test):
        n_tests = rate_test.shape[0]
        temp = [] # squared error
        for n in range(n_tests):
            user = self.predList['user'] == rate_test[n, 0]
            item = self.predList['item'] == rate_test[n, 1]
            pred = self.predList[user & item]
            pred = pred.iloc[0]['rating']
            temp.append(pred)       
        MAE = mean_absolute_error(rate_test[:,2], temp)   
        return MAE
    
    def evaluate_MSE(self, rate_test):
        n_tests = rate_test.shape[0]
        temp = [] # squared error
        for n in range(n_tests):
            user = self.predList['user'] == rate_test[n, 0]
            item = self.predList['item'] == rate_test[n, 1]
            pred = self.predList[user & item]
            pred = pred.iloc[0]['rating']
            temp.append(pred)      
        MSE = mean_squared_error(rate_test[:,2], temp)   
        return MSE
            
    
    def recommend(self, u):
        ids = np.where(self.Y_data[:, 0] == u)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()              
        recommended_items = []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                rating = self.__pred(u, i)
                if rating > 0: 
                    recommended_items.append(i)
        
        return recommended_items 
    
    def full(self):
        self.predList = []
        for u in range(self.n_users):
            ids = np.where(self.Y_data[:, 0] == u)[0]
            items_rated_by_u = self.Y_data[ids, 1].tolist() 
            for i in range(self.n_items):
                if i not in items_rated_by_u:
                    temp = [];
                    rating = self.__pred(u, i)
                    temp.append(u)
                    temp.append(i)
                    temp.append(rating)
                    self.predList.append(temp)
        labels = ['user', 'item', 'rating']
        self.predList = pd.DataFrame.from_records(self.predList, columns=labels)
        
    def pred_for_user(self, user_id):
        ids = np.where(self.Y_data[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()                    
        predicted_ratings= []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                predicted_ratings.append((i, self.__pred(user_id,i)))     
        return predicted_ratings
    
    def null_rated_item(self, user_id):
        ids = np.where(self.Y_data[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()                    
        #y_pred = self.X.dot(self.W[:, user_id])
        null_rated= []
        for i in range(self.n_items):
            if i in items_rated_by_u:
                null_rated.append((i, -1))     
        return null_rated
    
    def get_result_item(self, user_id):
        ids = np.where(self.Y_test[:, 0] == user_id)[0]
        ids = self.Y_test[ids]
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        id_item = ids[:,1].tolist()   
        data_item = ids[:,2].tolist()  
        list_result = []
        temp = 0 
        for i in range(52):
            if i not in id_item:
                list_result.append((i, -1)) 
            if i in id_item:
                list_result.append((i, data_item[temp]))
                temp = temp + 1
        return list_result
    
    def get_item_null(self, user_id):
        ids = np.where(self.Y_data[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()                    
        list_null = []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                list_null.append((i, -1))     
        return list_null
                
# data file 
r_cols = ['stt','user_id', 'item_id', 'rating']
ratings = pd.read_csv('data.csv', names = r_cols, encoding='latin-1')
ratings = ratings.drop('stt', 1)
ratings = ratings.loc[1:,:]
ratings = ratings.apply(pd.to_numeric)
ratings = ratings.as_matrix()

testing = pd.read_csv('testing.csv', names = r_cols, encoding='latin-1')
testing = testing.drop('stt', 1)
testing = testing.loc[1:,:]
testing = testing.apply(pd.to_numeric)
testing = testing.as_matrix()

training = pd.read_csv('training.csv', names = r_cols, encoding='latin-1')
training = training.drop('stt', 1)
training = training.loc[1:,:]
training = training.apply(pd.to_numeric)
training = training.as_matrix()

from sklearn.model_selection import train_test_split
X_train, X_test = train_test_split(ratings, test_size=0.3, random_state=42)

rs = CF(X_train, X_test, k = 10, uuCF = 1)
rs.fit()
rs.full()

print("--------Ma trận độ tương đồng------------------")
print(rs.S)
print("--------------------------")
print('RMSE: ', rs.evaluate_RMSE(X_test)*2)
print('MAE: ', rs.evaluate_MAE(X_test)*2)
print('MSE: ', rs.evaluate_MSE(X_test)*2)

import pickle
filename = 'TDMUModel_CF.sav'
#Save to file in the current working directory
pickle_out = open(filename,"wb")
pickle.dump(rs, pickle_out)
pickle_out.close()
