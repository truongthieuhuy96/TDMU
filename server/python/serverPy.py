# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 17:14:19 2018

@author: Administrator
"""

from flask import Flask, jsonify, request, abort, make_response
from flask_cors import CORS
import pickle
import numpy as np
import json

#class definition for vm object
class vm:
  def __init__(self,idmonhoc,dudoan):
    self.idmonhoc=idmonhoc
    self.dudoan=dudoan

app = Flask(__name__)
CORS(app)
filename = 'TDMUModel.sav'
filename_ = 'TDMUModel_.sav'
filenameCF = 'TDMUModel_CF.sav'
filenameCF_ = 'TDMUModel_CF_.sav'
filenameCB = 'TDMUModel_CB.sav'
filenameCB_ = 'TDMUModel_CB_.sav'

@app.route('/')
def index():
    return "Hello, World!"

'''Matrix Factorization'''
@app.route('/api/full', methods=['POST'])
def full(): 
    if not request.json or not 'model' in request.json or not 'masv' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filename,'rb'))
    else:
        model = pickle.load(open(filename_,'rb'))
    idsv = int(request.json['masv'])
    ids = np.where(model.Y_raw[:, 0] == idsv)[0]
    test = model.Y_raw[ids]
    data = model.pred_for_user(idsv)
    temp =  0
    for i in test:
        test[temp][2] = -i[2]
        temp += 1
    rul = np.append(test[:,1:],np.asarray(data), axis=0)
    rul = rul[rul[:,0].argsort(kind='mergesort')]   
    class vm:
        def __init__(self,diem):
            self.diem=diem
    #create some VMs and add them to a list
    vmlist= []
    for i in rul:
        myvm=i[1]
        vmlist.append(myvm)
    return jsonify({'data': vmlist}), 200
        
@app.route('/api/predict', methods=['POST'])
def predict():
    if not request.json or not 'idsv' in request.json:
        abort(400)
    model = pickle.load(open(filename,'rb'))
    result = model.pred(request.json['idsv'],request.json['idmon'])
    return jsonify({'data': result*2}), 200

@app.route('/api/percentdata', methods=['POST'])
def precent():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filename,'rb'))
    else:
        model = pickle.load(open(filename_,'rb'))
    return jsonify({'traning': len(model.Y_data),'test': len(model.Y_test)}), 200

@app.route('/api/predict/all', methods=['POST'])
def predict_all():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filename,'rb'))
    else:
        model = pickle.load(open(filename_,'rb'))
    list_data = []
    for x in range(0,32):
        ids = model.null_rated_item(x)
        ids = np.array(ids)
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        test = model.pred_for_user(x)
        data = np.append(ids,np.asarray(test), axis=0)
        data = data[data[:,0].argsort(kind='mergesort')]
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in data:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/data', methods=['POST'])
def data():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filename,'rb'))
    else:
        model = pickle.load(open(filename_,'rb'))
    list_data = []
    for x in range(0,32):
        ids = np.where(model.Y_raw[:, 0] == x)[0]
        ids = model.Y_raw[ids]
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        test = model.get_item_null(x)
        rul = np.append(ids[:,1:],np.asarray(test), axis=0)
        rul = rul[rul[:,0].argsort(kind='mergesort')]       
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in rul:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/evaluate', methods=['POST'])
def evaluate():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filename,'rb'))
    else:
        model = pickle.load(open(filename_,'rb'))
    test_data = model.Y_test
    rmse = model.evaluate_RMSE(test_data)
    mae = model.evaluate_MAE(test_data)
    mse = model.evaluate_MSE(test_data)
    return jsonify({'rmse': rmse,'mae': mae, 'mse': mse}), 200

@app.route('/api/predict/result', methods=['POST'])
def result():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filename,'rb'))
    else:
        model = pickle.load(open(filename_,'rb'))
    list_data = []
    for x in range(0,32):
        test = model.get_result_item(x)   
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in test:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

''' Collaborative Filtering'''
@app.route('/api/predict/cf/percentdata', methods=['POST'])
def cf_percent():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCF,'rb'))
    else:
        model = pickle.load(open(filenameCF_,'rb'))
    return jsonify({'traning': len(model.Y_data),'test': len(model.Y_test)}), 200

@app.route('/api/predict/cf/all', methods=['POST'])
def cf_predict_all():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCF,'rb'))
    else:
        model = pickle.load(open(filenameCF_,'rb'))
    list_data = []
    for x in range(0,32):
        ids = model.null_rated_item(x)
        ids = np.array(ids)
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        test = model.pred_for_user(x)
        data = np.append(ids,np.asarray(test), axis=0)
        data = data[data[:,0].argsort(kind='mergesort')]
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in data:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/cf/result', methods=['POST'])
def cf_result():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCF,'rb'))
    else:
        model = pickle.load(open(filenameCF_,'rb'))
    list_data = []
    for x in range(0,32):
        test = model.get_result_item(x)   
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in test:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/cf/evaluate', methods=['POST'])
def cf_evaluate():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCF,'rb'))
    else:
        model = pickle.load(open(filenameCF_,'rb'))
    test_data = model.Y_test
    rmse = model.evaluate_RMSE(test_data)
    mae = model.evaluate_MAE(test_data)
    mse = model.evaluate_MSE(test_data)
    return jsonify({'rmse': rmse,'mae': mae, 'mse': mse}), 200

@app.route('/api/predict/cf/data', methods=['POST'])
def cf_data():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCF,'rb'))
    else:
        model = pickle.load(open(filenameCF_,'rb'))
    list_data = []
    for x in range(0,32):
        ids = np.where(model.Y_data[:, 0] == x)[0]
        ids = model.Y_data[ids]
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        test = model.get_item_null(x)
        rul = np.append(ids[:,1:],np.asarray(test), axis=0)
        rul = rul[rul[:,0].argsort(kind='mergesort')]       
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in rul:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/cf/full', methods=['POST'])
def cf_full(): 
    if not request.json or not 'model' in request.json or not 'masv' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCF,'rb'))
    else:
        model = pickle.load(open(filenameCF_,'rb'))
    idsv = int(request.json['masv'])
    ids = np.where(model.Y_data[:, 0] == idsv)[0]
    test = model.Y_data[ids]
    data = model.pred_for_user(idsv)
    temp =  0
    for i in test:
        test[temp][2] = -i[2]
        temp += 1
    rul = np.append(test[:,1:],np.asarray(data), axis=0)
    rul = rul[rul[:,0].argsort(kind='mergesort')]   
    class vm:
        def __init__(self,diem):
            self.diem=diem
    #create some VMs and add them to a list
    vmlist= []
    for i in rul:
        myvm=i[1]
        vmlist.append(myvm)
    return jsonify({'data': vmlist}), 200

'''Content Base'''
@app.route('/api/predict/cb/percentdata', methods=['POST'])
def cb_percent():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCB,'rb'))
    else:
        model = pickle.load(open(filenameCB_,'rb'))
    return jsonify({'traning': len(model.Y_data),'test': len(model.Y_test)}), 200

@app.route('/api/predict/cb/all', methods=['POST'])
def cb_predict_all():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCB,'rb'))
    else:
        model = pickle.load(open(filenameCB_,'rb'))
    list_data = []
    for x in range(0,32):
        ids = model.null_rated_item(x)
        ids = np.array(ids)
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        test = model.pred_for_user(x)
        data = np.append(ids,np.asarray(test), axis=0)
        data = data[data[:,0].argsort(kind='mergesort')]
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in data:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/cb/result', methods=['POST'])
def cb_result():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCB,'rb'))
    else:
        model = pickle.load(open(filenameCB_,'rb'))
    list_data = []
    for x in range(0,32):
        test = model.get_result_item(x)   
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in test:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/cb/evaluate', methods=['POST'])
def cb_evaluate():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCB,'rb'))
    else:
        model = pickle.load(open(filenameCB_,'rb'))
    rmse = model.evaluate_RMSE()
    mae = model.evaluate_MAE()
    mse = model.evaluate_MSE()
    return jsonify({'rmse': rmse,'mae': mae, 'mse': mse}), 200

@app.route('/api/predict/cb/data', methods=['POST'])
def cb_data():
    if not request.json or not 'model' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCB,'rb'))
    else:
        model = pickle.load(open(filenameCB_,'rb'))
    list_data = []
    for x in range(0,32):
        ids = np.where(model.Y_data[:, 0] == x)[0]
        ids = model.Y_data[ids]
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        test = model.get_item_null(x)
        rul = np.append(ids[:,1:],np.asarray(test), axis=0)
        rul = rul[rul[:,0].argsort(kind='mergesort')]       
        class vm:
            def __init__(self,diem):
                self.diem=diem
        #create some VMs and add them to a list
        vmlist= []
        for i in rul:
            myvm=i[1]
            vmlist.append(myvm)
        list_data.append(vmlist)
    #convert dictionary list to json    
    jsondata=json.dumps(list_data)
    return jsonify({'data': jsondata}), 200

@app.route('/api/predict/cb/full', methods=['POST'])
def cb_full(): 
    if not request.json or not 'model' in request.json or not 'masv' in request.json:
        abort(400)
    if request.json['model'] == "1":
        model = pickle.load(open(filenameCB,'rb'))
    else:
        model = pickle.load(open(filenameCB_,'rb'))
    idsv = int(request.json['masv'])
    ids = np.where(model.Y_data[:, 0] == idsv)[0]
    test = model.Y_data[ids]
    data = model.pred_for_user(idsv)
    temp =  0
    for i in test:
        test[temp][2] = -i[2]
        temp += 1
    rul = np.append(test[:,1:],np.asarray(data), axis=0)
    rul = rul[rul[:,0].argsort(kind='mergesort')]   
    class vm:
        def __init__(self,diem):
            self.diem=diem
    #create some VMs and add them to a list
    vmlist= []
    for i in rul:
        myvm=i[1]
        vmlist.append(myvm)
    return jsonify({'data': vmlist}), 200
    
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
     app.run(port = 3000, debug = False)