# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 07:19:11 2018

@author: Administrator
"""

import pandas as pd 
import numpy as np
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

class MF(object):
    """docstring for CF"""
    def __init__(self, Y_data, Y_test, K, lam = 0.1, Xinit = None, Winit = None, 
                 learning_rate = 0.5, max_iter = 1000, print_every = 100, user_based = 1):
        self.Y_test = Y_test.copy()
        self.Y_raw = Y_data.copy()
        self.Y_data = Y_data.copy()
        self.K = K
        self.lam = lam
        self.learning_rate = learning_rate
        self.max_iter = max_iter
        self.print_every = print_every
        self.user_based = user_based
        # number of users and items. Remember to add 1 since id starts from 0
        self.n_users = 32 #int(np.max(Y_data[:, 0])) + 1
        self.n_items = 52 #int(np.max(Y_data[:, 1])) + 1
        
        if Xinit is None: 
            self.X = np.random.randn(self.n_items, K)
        else:
            self.X = Xinit 
        
        if Winit is None: 
            self.W = np.random.randn(K, self.n_users)
        else: 
            self.W = Winit
        
        # item biases
        self.b = np.random.randn(self.n_items)
        self.d = np.random.randn(self.n_users)
        self.n_ratings = Y_data.shape[0]
        self.mu = 0
 

    def normalize_Y(self):
        if self.user_based:
            user_col = 0
            item_col = 1
            n_objects = self.n_users
        else:
            user_col = 1
            item_col = 0 
            n_objects = self.n_items

        users = self.Y_data[:, user_col] 
        self.muu = np.zeros((n_objects,))
        for n in range(n_objects):
            # row indices of rating done by user n
            # since indices need to be integers, we need to convert
            ids = np.where(users == n)[0].astype(np.int32)
            # indices of all ratings associated with user n
            item_ids = self.Y_data[ids, item_col] 
            # and the corresponding ratings 
            ratings = self.Y_data[ids, 2]
          
            # take mean
            m = np.mean(ratings) 
#             print m
            if np.isnan(m):
                m = 0 # to avoid empty array and nan value
            self.muu[n] = m
            # normalize
            self.Y_data[ids, 2] = ratings - m
    
    def recommend(self, u):
        """
        Determine all items should be recommended for user u.
        The decision is made based on all i such that:
        self.pred(u, i) > 0. Suppose we are considering items which 
        have not been rated by u yet. 
        """
        ids = np.where(self.Y_data[:, 0] == u)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()              
        recommended_items = []
    
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                rating = self.pred(u, i)
                if rating > 3: 
                    recommended_items.append(i)
        return recommended_items 
            
            
    def loss(self):
        L = 0 
        for i in range(self.n_ratings):
            # user, item, rating
            n, m, rate = int(self.Y_data[i, 0]), int(self.Y_data[i, 1]), self.Y_data[i, 2]
            L += 0.5*(self.X[m, :].dot(self.W[:, n]) + self.b[m] + self.d[n] + self.mu - rate)**2
            
        # regularization, don't ever forget this 
        L /= self.n_ratings
        L += 0.5*self.lam*(np.linalg.norm(self.X, 'fro') + np.linalg.norm(self.W, 'fro') + \
                          np.linalg.norm(self.b) + np.linalg.norm(self.d))
        return L 

    
    def get_items_rated_by_user(self, user_id):
        """
        get all items which are rated by user n, and the corresponding ratings
        """
        # y = self.Y_data_n[:,0] # all users (may be duplicated)
        # item indices rated by user_id
        # we need to +1 to user_id since in the rate_matrix, id starts from 1 
        # while index in python starts from 0
        ids = np.where(self.Y_data[:,0] == user_id)[0] 
        item_ids = self.Y_data[ids, 1].astype(np.int32) # index starts from 0 
        ratings = self.Y_data[ids, 2]
        return (item_ids, ratings)
    
    def get_item_null(self, user_id):
        ids = np.where(self.Y_data[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()                    
        list_null = []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                list_null.append((i, -1))     
        return list_null
    
    def get_result_item(self, user_id):
        ids = np.where(self.Y_test[:, 0] == user_id)[0]
        ids = self.Y_test[ids]
        ids = ids[ids[:,1].argsort(kind='mergesort')]
        id_item = ids[:,1].tolist()   
        data_item = ids[:,2].tolist()  
        list_result = []
        temp = 0 
        for i in range(52):
            if i not in id_item:
                list_result.append((i, -1)) 
            if i in id_item:
                list_result.append((i, data_item[temp]))
                temp = temp + 1
        return list_result
        
        
    def get_users_who_rate_item(self, item_id):
        """
        get all users who rated item m and get the corresponding ratings
        """
        ids = np.where(self.Y_data[:,1] == item_id)[0] 
        user_ids = self.Y_data[ids, 0].astype(np.int32)
        ratings = self.Y_data[ids, 2]
        return (user_ids, ratings)
        
    def updateX(self):
        for m in range(self.n_items):
            user_ids, ratings = self.get_users_who_rate_item(m)
            
            Wm = self.W[:, user_ids]
            dm = self.d[user_ids]
            xm = self.X[m, :]
            
            error = xm.dot(Wm) + self.b[m] + dm + self.mu - ratings 
            
            grad_xm = error.dot(Wm.T)/self.n_ratings + self.lam*xm
            grad_bm = np.sum(error)/self.n_ratings + self.lam*self.b[m]
            self.X[m, :] -= self.learning_rate*grad_xm.reshape((self.K,))
            self.b[m]    -= self.learning_rate*grad_bm
    
    def updateW(self):
        for n in range(self.n_users):
            item_ids, ratings = self.get_items_rated_by_user(n)
            Xn = self.X[item_ids, :]
            bn = self.b[item_ids]
            wn = self.W[:, n]
            
            error = Xn.dot(wn) + bn + self.mu + self.d[n] - ratings
            grad_wn = Xn.T.dot(error)/self.n_ratings + self.lam*wn
            grad_dn = np.sum(error)/self.n_ratings + self.lam*self.d[n]
            self.W[:, n] -= self.learning_rate*grad_wn.reshape((self.K,))
            self.d[n]    -= self.learning_rate*grad_dn
    
    def fit(self):
        self.normalize_Y()
        for it in range(self.max_iter):
            self.updateX()
            self.updateW()
            if (it + 1) % self.print_every == 0:
                rmse_train = self.evaluate_RMSE(self.Y_raw)
                print ('iter =', it + 1, ', loss =', self.loss(), ', RMSE train =', rmse_train)
    
    
    def pred(self, u, i):
        """ 
        predict the rating of user u for item i 
        if you need the un
        """
        u = int(u)
        i = int(i)
        if self.user_based == 1:
            bias = self.muu[u]
        else:
            bias = self.muu[i]   
        pred = self.X[i, :].dot(self.W[:, u]) + self.b[i] + self.d[u] + bias
        return max(0, min(5, pred))
    
    def pred_all(self):
        list_data = []
        for i in range(self.n_users):
            temp = self.pred_for_user(i)
            list_data.append(temp)
        return list_data
    
    def null_rated_item(self, user_id):
        ids = np.where(self.Y_data[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()                    
        #y_pred = self.X.dot(self.W[:, user_id])
        null_rated= []
        for i in range(self.n_items):
            if i in items_rated_by_u:
                null_rated.append((i, -1))     
        return null_rated
    
    def pred_for_user(self, user_id):
        ids = np.where(self.Y_data[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()                    
        #y_pred = self.X.dot(self.W[:, user_id])
        predicted_ratings= []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                predicted_ratings.append((i, self.pred(user_id,i)))     
        return predicted_ratings
    
    def evaluate_RMSE(self, rate_test):
        n_tests = rate_test.shape[0]
        SE = 0 # squared error
        for n in range(n_tests):
            pred = self.pred(rate_test[n, 0], rate_test[n, 1])
            SE += (pred - rate_test[n, 2])**2 

        RMSE = np.sqrt(SE/n_tests)
        return RMSE
    
    def evaluate_MAE(self, rate_test):
        n_tests = rate_test.shape[0]
        temp = [] # squared error
        for n in range(n_tests):
            temp.append(self.pred(rate_test[n, 0], rate_test[n, 1]))       
        MAE = mean_absolute_error(rate_test[:,2], temp)   
        return MAE
    
    def evaluate_MSE(self, rate_test):
        n_tests = rate_test.shape[0]
        temp = [] # squared error
        for n in range(n_tests):
            temp.append(self.pred(rate_test[n, 0], rate_test[n, 1]))       
        MSE = mean_squared_error(rate_test[:,2], temp)   
        return MSE
    
    #def evalute(self, u, i):
    #    id_user = np.where(self.Y_data[:, 0] == u)[0]
    #    id_mon = np.where(self.Y_data[:, 1] == i)[0]      
    #    predict = self.pred(u,i)
        
r_cols = ['stt','user_id', 'item_id', 'rating']
ratings = pd.read_csv('data.csv', names = r_cols, encoding='latin-1')
ratings = ratings.drop('stt', 1)
ratings = ratings.loc[1:,:]
ratings = ratings.apply(pd.to_numeric)
ratings = ratings.as_matrix()

testing = pd.read_csv('testing.csv', names = r_cols, encoding='latin-1')
testing = testing.drop('stt', 1)
testing = testing.loc[1:,:]
testing = testing.apply(pd.to_numeric)
testing = testing.as_matrix()

training = pd.read_csv('training.csv', names = r_cols, encoding='latin-1')
training = training.drop('stt', 1)
training = training.loc[1:,:]
training = training.apply(pd.to_numeric)
training = training.as_matrix()

#----------------------------
from sklearn.model_selection import train_test_split
X_train, X_test = train_test_split(ratings, test_size=0.3, random_state=42)
#X_train = X_train.apply(pd.to_numeric)
#X_test = X_test.apply(pd.to_numeric)
#print (X_train.shape, X_test.shape)
#------------------------------
#Y_data = ratings.as_matrix()
#rs = MF(Y_data, K = 2, max_iter = 1000, print_every = 1000)
#rs.fit()int(np.max(Y_data[:, 0])) + 1 
rs = MF(X_train, X_test, K = 10, lam = 0.005, print_every = 5, learning_rate = 2, max_iter = 100, user_based = 1)
rs.fit()
#print(rs.X)
#evaluate on test data
#RMSE = rs.evaluate_RMSE(testing)
#print ('\nItem-based MF, RMSE =', RMSE)
#print(rs.get_items_rated_by_user(6))
#print(rs.pred_for_user(6))
#print('RMSE: ', rs.evaluate_RMSE(X_test)*2)
#print('MAE: ', rs.evaluate_MAE(X_test)*2)
#print('MSE: ', rs.evaluate_MSE(X_test)*2)


import pickle
filename = 'TDMUModel.sav'
#Save to file in the current working directory
pickle_out = open(filename,"wb")
pickle.dump(rs, pickle_out)
pickle_out.close()