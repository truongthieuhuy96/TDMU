var Sinhvien = require('../models/sinhviens.model');
var User = require('../models/users.model');
var crypto = require('crypto');
var secret = 'meomeomeo';

module.exports = {
    getSinhvien: getSinhvien,
    getSinhviens: getSinhviens,
    createSinhvien: createSinhvien,
    updateSinhvien: updateSinhvien,
    newSinhvien: newSinhvien
}

function newSinhvien(request) {
    return new Promise((resolve,reject)=>{
        var sinhvien = new Sinhvien(request);
        sinhvien.save((err,res)=>{
            if(err)
                reject({
                    message: err
                })
            else {
                resolve({
                    message: 'Ok'
                })
            }
        })
    })
}

function getSinhviens() {
    return new Promise((resolve, reject) => {
        Sinhvien.find({}, { id_user: 0 }).exec(function (err, res) {
            if (err) {
                reject(err);
            }
            else {
                resolve(res);
            };
        });
    });
}

function getSinhvien(request) {
    return new Promise((resolve, reject) => {
        Sinhvien.findById(request, { id_user: 0 }).exec(function (err, res) {
            if (err) {
                reject(err);
            }
            else {
                resolve(res);
            };
        });
    });
}

function createSinhvien(request) {
    return new Promise((resolve, reject) => {
        var sinhvien = new Sinhvien({
            idsinhvien: Sinhvien.find({}).count(),
            tensinhvien: request.tensinhvien,
            masinhvien: request.masinhvien,
            lop: request.lop,
            khoa: request.khoa,
            nganh: request.nganh
        });
        sinhvien.save(function (err, res) {
            if (err) {
                reject({
                    statusCode: 400,
                    message: 'Register failure!!'
                });
            }
            else {
                var hash = crypto.createHmac('sha256', secret)
                    .update(sinhvien.masinhvien)
                    .digest('hex');
                var user = new User({
                    username: sinhvien.masinhvien,
                    password: hash,
                    idsinhvien: sinhvien._id
                })
                user.save((err, res) => {
                    if (err) {
                        reject({
                            statusCode: 400,
                            message: 'Register failure!!'
                        });
                    }
                    else {
                        Sinhvien.findOneAndUpdate({ _id: sinhvien._id }, { $set: { id_user: user._id } }).then((res) => {
                            resolve({
                                message: 'Register success!!'
                            })
                        }).catch((err) => {
                            reject({
                                statusCode: 400,
                                message: 'Register failure!!'
                            });
                        })
                    }
                })
            }
        })
    })
}

function updateSinhvien(request) {
    return new Promise((resolve, reject) => {
        Sinhvien.findByIdAndUpdate(request.id, { $set: request.data }, function (err, res) {
            if (err) {
                reject({
                    statusCode: 400,
                    message: 'Update failure!!'
                });
            }
            else {
                resolve({
                    statusCode: 200,
                    message: 'Update success!!'
                });
            }
        });
    });
}