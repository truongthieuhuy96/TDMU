var Monhoc = require('../models/monhocs.model');

module.exports = {
    getMonhocs: getMonhocs,
    createMonhoc: createMonhoc,
    updateMonhoc: updateMonhoc
}

function getMonhocs() {
    return new Promise((resolve, reject) => {
        Monhoc.find({}).sort({idmonhoc: 1}).exec(function (err, res) {
            if (err) {
                reject(err);
            }
            else {
                resolve(res);
            };
        });
    });
}

function createMonhoc(request) {
    return new Promise((resolve, reject) => {
        Monhoc.count({}, (err, data) => {
            if (data) {
                var count = data;
                var monhoc = new Monhoc({
                    idmonhoc: count,
                    mamonhoc: request.mamonhoc,
                    tenmonhoc: request.tenmonhoc,
                    monhoctienquyet: request.monhoctienquyet,
                    sotinchi: request.sotinchi
                });
                monhoc.save(function (err, res) {
                    if (err) {
                        reject({
                            statusCode: 400,
                            message: 'Lỗi thêm môn học!!!'
                        });
                    }
                    else {
                        resolve({
                            statusCode: 200,
                            message: 'Thêm môn học thành công!!!'
                        })
                    }
                });
            }
        });
    })
}

function updateMonhoc(request) {
    return new Promise((resolve, reject) => {
        Monhoc.findByIdAndUpdate(request.id, { $set: request.data }, function (err, res) {
            if (err) {
                reject({
                    statusCode: 400,
                    message: 'Update failure!!'
                });
            }
            else {
                resolve({
                    statusCode: 200,
                    message: 'Update success!!'
                });
            }
        });
    });
}