var User = require('../models/users.model');
var crypto = require('crypto');
var jwt = require('../utils/jwt');
var secret = 'meomeomeo';
var config = require("../config");

module.exports = {
    login: login,
    checkAuth: checkAuth
}

function login(username, password) {
    var hash = crypto.createHmac('sha256', secret)
        .update(password)
        .digest('hex');

    return User.findOne({
        username: username,
        password: hash
    }).populate({ path: 'idsinhvien', select: 'tensinhvien' })
        .then(function (user) {
            if (user) {
                return new Promise(function (resolve, reject) {
                    jwt.sign({
                        username: user.username
                    }, function (err, token) {
                        if (err) {
                            reject({
                                statusCode: 400,
                                message: err.message
                            });
                        } else {
                            resolve({
                                token: token,
                                username: user.idsinhvien.tensinhvien,
                                avatar: `${config.server.domain}:${config.server.port}/avatars/${user.avatar}`
                            });
                        }
                    })
                });

            } else {
                return Promise.reject({
                    statusCode: 400,
                    message: 'Email or password is incorrect'
                });
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function checkAuth(user) {
    return User.findOne(user)
        .then(function (foundUser) {
            if (foundUser) {
                return Promise.resolve(foundUser);
            } else {
                return Promise.reject({
                    message: 'Not Found'
                });
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}