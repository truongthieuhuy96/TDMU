var Users = require('./../models/users.model');
var crypto = require('crypto');
var secret = 'meomeomeo';
var config = require("../config");
var path = require('path');
var mail = require('../utils/mail');

module.exports = {
    getUsers: getUsers,
    createUser: createUser,
    updateUser: updateUser,
    deleteUser: deleteUser,
    uploadAvatar: uploadAvatar
}

function uploadAvatar(userId, file) {
    //find user to upload avatar
    return Users.findOne({ _id: userId })
        .then(function (user) {
            if (user) {
                return new Promise(function (resolve, reject) {
                    //move to avatar folder
                    file.mv(path.join(__dirname, '../public/avatars/avatar_' + user._id + '.png'), function (err) {
                        if (err)
                            reject(err);

                        //update current user with new avatar path
                        return Users.findOneAndUpdate({ _id: userId }, { $set: { avatar: 'avatar_' + user._id + '.png' } })
                            .then(function (data) {
                                resolve(`${config.server.domain}:${config.server.port}/avatars/avatar_${user._id}.png`);
                            })
                            .then(function (err) {
                                reject(err);
                            })
                    });
                });

            } else {
                return Promise.reject({
                    message: "Not Found",
                    statusCode: 404
                });
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function getUsers(request) {
    return new Promise((resolve, reject) => {
        Users.findById(request, { password: 0 }).exec(function (err, res) {
            if (err) {
                reject(err);
            }
            else {
                resolve(res);
            };
        });
    });
}

function getUsersWithCondition(request) {
    var q = {};
    var sort = {};
    if (request.search) {
        q["$text"] = { $search: request.search };
    }
    for (var p in request) {
        if (p != "page" && p != "limit" && p != "search") {
            sort[p] = parseInt(request[p]);
        }
    }
    console.log(request['search']);
    console.log(q);
    console.log(sort);
    return new Promise((resolve, reject) => {
        Users.find(q, { password: 0 })
            .limit(parseInt(request.limit))
            .skip(parseInt(request.limit) * (parseInt(request.page) - 1))
            .sort(sort)
            .exec(function (err, res) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(res);
                }
            })
    })
}

function createUser(request) {
    return new Promise((resolve, reject) => {
        Users.find({ username: request.username }).then(function (res) {
            if (res.length > 0) {
                reject({
                    statusCode: 400,
                    message: 'Username is existed'
                });
            }
            else {
                var hash = crypto.createHmac('sha256', secret)
                    .update(request.password)
                    .digest('hex');
                request.password = hash;
                var user = new Users(request);
                user.save(function (err, res) {
                    if (err) {
                        reject({
                            statusCode: 400,
                            message: 'Register failure!!'
                        });
                    }
                    else {
                        mail.sendMail('', user.email, 'New user registration', '<h1>Wellcome to the APP</h1>')
                            .then(function (res) {
                                resolve({
                                    statusCode: 200,
                                    message: 'Register success!!'
                                });
                            })
                            .catch(function (err) {
                                reject(err);
                            });
                    }
                });
            }
        });
    });
}

function updateUser(request) {
    return new Promise((resolve, reject) => {
        Users.findByIdAndUpdate(request.id, { $set: request.data }, function (err, res) {
            if (err) {
                reject({
                    statusCode: 400,
                    message: 'Update failure!!'
                });
            }
            else {
                resolve({
                    statusCode: 200,
                    message: 'Update success!!'
                });
            }
        });
    });
}

function deleteUser(request) {
    return new Promise((resolve, reject) => {
        Users.findByIdAndRemove(request, function (err, res) {
            if (err) {
                reject({
                    statusCode: 400,
                    message: 'Delete failure!!!'
                });
            }
            else {
                resolve({
                    statusCode: 200,
                    message: 'Delete success!!!'
                });
            }
        });
    });
}