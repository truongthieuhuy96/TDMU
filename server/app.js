var express = require('express');
var app = express();
var db = require('./db/mongodb');
var config = require("./config");
var bodyParser = require('body-parser');
var auth = require('./middle-wave/auth');
var errorHandler = require('./middle-wave/errorhandler');
var fileUpload = require('express-fileupload');
//Route require
var userRoute = require('./routes/user.route');
var loginRoute = require('./routes/login.route');
var sinhvienRoute = require('./routes/sinhvien.route');
var monhocRoute = require('./routes/monhoc.route');

//enabel CORS
app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');
    next();
});

//Default options middlewave
app.use(fileUpload());

//Body Parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Middlewave
app.use(function(req,res,next){
    console.log('Begin middlewave');
    next();
})

//Static
app.use(express.static('public'));

//Route
app.use(config.BASE_URL, loginRoute);
app.use(config.BASE_URL, userRoute);
app.use(config.BASE_URL, sinhvienRoute);
app.use(config.BASE_URL, monhocRoute);

//ErrorHandler
app.use(errorHandler.errorHandler());

app.listen(config.server.port, function () {
    console.log('Server listening port:'+ config.server.port);
});