var router = require('express').Router();
var fs = require('fs');
var path = require('path');
var auth = require('./../middle-wave/auth');
var userDao = require('./../dao/users.dao');

router.get('/users',auth.auth(), getUser);
router.post('/users', createUser);
router.put('/users',auth.auth(), updateUser);
router.delete('/users',auth.auth(), deleteUser);
router.post('/avatar', auth.auth(), uploadAvatar);

module.exports = router;

function uploadAvatar(req, res, next) {
    if (!req.files)
        return next({
            message: 'No files were uploaded.'
        });

    var uploadedFile = req.files.file;

    userDao.uploadAvatar(req.user._id, uploadedFile)
        .then(function (avatar) {
            res.send({
                avatar: avatar
            })
        })
        .catch(function (err) {
            next(err);
        })
}

function getUser(req, res, next) {
    userDao.getUsers(req.user._id).then(function (response) {
        res.send(response);
    }).catch(function (err) {
        next(err);
    });
}

function createUser(req, res, next) {

    // fs.readFile(path.join(__dirname, "../" + "data.json"), 'utf-8', function (err, data) {
    //     var list = JSON.parse(data);
    //     var newUser = {
    //         username: req.body.username,
    //         password: req.body.password,
    //         id: list[list.length-1].id + 1
    //     }

    //     list.push(newUser);

    //     fs.writeFile(path.join(__dirname, "../" + "data.json"), JSON.stringify(list), function (err, data) {
    //         if (err) {
    //             res.status(500);
    //             res.end("Error");
    //         }
    //         else {
    //             res.send(list);
    //         }
    //     })
    // })
    var request = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        permission: req.body.permission,
        date: Date.now()
    };
    userDao.createUser(request).then(function (response) {
        res.send(response);
    }).catch(function (err) {
        next(err);
    });
}

function updateUser(req, res, next) {

    // fs.readFile(path.join(__dirname, "../" + "data.json"), 'utf-8', function (err, data) {
    //     var list = JSON.parse(data);
    //     for (var i = 0; i < list.length; i++) {
    //         if (list[i].id == id) {
    //             list[i].username = newUser.username;
    //             list[i].password = newUser.password;
    //             break;
    //         }
    //     }
    //     fs.writeFile(path.join(__dirname + "/" + "data.json"), JSON.stringify(list), function (err, data) {
    //         if (err) {
    //             res.status(500);
    //             res.end("Error");
    //         }
    //         else {
    //             res.send(list);
    //         }
    //     })
    // })
    var request = {
        id: req.user._id,
        data: req.body
    }
    userDao.updateUser(request).then(function (response) {
        res.send(response);
    }).catch(function (err) {
        next(err);
    });
}

function deleteUser(req, res) {
    // var newUser = req.body;
    // var id = req.params.id;

    // fs.readFile(path.join(__dirname, "../" + "data.json"), 'utf-8', function (err, data) {
    //     var list = JSON.parse(data);
    //     if (id > -1) {
    //         for (var i = 0; i < list.length; i++) {
    //             if (list[i].id == id) {
    //                 list.splice(id, 1);
    //                 break;
    //             }
    //         }
    //     }
    //     fs.writeFile(path.join(__dirname, "../" + "data.json"), JSON.stringify(list), function (err, data) {
    //         if (err) {
    //             res.status(500);
    //             res.end("Error");
    //         }
    //         else {
    //             res.send(list);
    //         }
    //     })
    // })
    userDao.deleteUser(req.user._id).then(function (response) {
        res.send(response);
    }).catch(function (err) {
        next(err);
    });
}