var router = require('express').Router();
var monhocDao = require('../dao/monhoc.dao');

router.get('/monhoc', getMonhocs)
router.post('/monhoc', createMonhoc);
router.put('/monhoc', updateMonhoc)

module.exports = router;

function getMonhocs(req, res, next) {
    monhocDao.getMonhocs().then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}

function createMonhoc(req, res, next) {
    monhocDao.createMonhoc(req.body).then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}

function updateMonhoc(req, res, next) {
    monhocDao.updateMonhoc(req.body).then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}

