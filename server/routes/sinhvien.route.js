var router = require('express').Router();
var sinhvienDao = require('../dao/sinhvien.dao');
var auth = require('./../middle-wave/auth');

router.get('/sinhviens', getSinhviens)
router.get('/sinhvien',auth.auth(), getSinhvien)
router.post('/sinhvien', createSinhvien);
router.put('/sinhvien', updateSinhvien);
router.post('/sinhvien/new', newSinhvien);

module.exports = router;

function newSinhvien(req, res, next) {
    sinhvienDao.newSinhvien(req.body).then(response => {
        res.send(response);
    }).catch(err => {
        next(err);
    })
}

function getSinhviens(req, res, next) {
    sinhvienDao.getSinhviens().then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}

function getSinhvien(req, res, next) {
    sinhvienDao.getSinhvien(req.user.idsinhvien).then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}

function createSinhvien(req, res, next) {
    sinhvienDao.createSinhvien(req.body).then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}

function updateSinhvien(req, res, next) {
    sinhvienDao.updateSinhvien(req.body).then((response)=>{
        res.send(response);
    }).catch((err)=>{
        next(err);
    })
}