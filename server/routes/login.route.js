var router = require('express').Router();
var authDao = require('../dao/auth.dao')


router.post('/login', login);

module.exports = router;

function login(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    if (!username) {
        next({
            statusCode: 400,
            message: "Email is required"
        })
    } else if (!password) {
        next({
            statusCode: 400,
            message: "Password is required"
        })
    } else {
        authDao.login(username, password).then(function (response) {
            res.send(response);
        }).catch(function (err) {
            next(err);
        });
    }
}