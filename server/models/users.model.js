var mongoose = require('mongoose');

// User Schema
var userSchema = mongoose.Schema({
    username: {
      type: String,
      required: true
    },
    idsinhvien: {type: mongoose.Schema.Types.ObjectId, ref: 'Sinhviens'},
    password:{
      type: String,
      required: true
    },
    email: {
      type: String
    },
    permission: {
      type: String,
      default: "Sinhvien"
    },
    date: Date,
    avatar: {
      type: String,
      default: "default.jpg"
    }
  });

  userSchema.index({username: 'text', permission: 'text'});
  
var Users = module.exports = mongoose.model('Users', userSchema);
