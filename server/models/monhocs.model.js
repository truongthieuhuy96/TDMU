var mongoose = require('mongoose');

// User Schema
var monhocSchema = mongoose.Schema({
    idmonhoc : {
        type: Number,
        required: true
    },
    mamonhoc: {
        type: String,
        required: true
    },
    tenmonhoc: {
        type: String,
        required: true
    },
    monhoctienquyet: {
        type: String,
        required: true
    },
    sotinchi: {
        type: String,
        required: true
    } 
});

monhocSchema.index({ tenmonhoc: 'text'});

var Monhocs = module.exports = mongoose.model('Monhocs', monhocSchema);
