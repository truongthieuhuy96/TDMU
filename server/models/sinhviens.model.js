var mongoose = require('mongoose');

// User Schema
var sinhvienSchema = mongoose.Schema({
  idsinhvien: {
    type: Number,
    required: true
  },
  tensinhvien: {
    type: String,
    required: true
  },
  masinhvien: {
    type: String,
    required: true
  },
  lop: {
    type: String,
    required: true
  },
  khoa: {
    type: String,
    required: true
  },
  nganh: {
    type: String,
    required: true
  },
  id_user: {type: mongoose.Schema.Types.ObjectId, ref: "Users"}
});

sinhvienSchema.index({ tensinhvien: 'text', masinhvien: 'text' });

var Sinhviens = module.exports = mongoose.model('Sinhviens', sinhvienSchema);
