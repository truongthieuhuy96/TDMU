var jwt = require('./../utils/jwt');
var fs = require('fs');
var path = require('path');
var authDao = require('../dao/auth.dao');

exports.auth = function () {
    return function (req, res, next) {
        var token = req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, function (err, decodedData) {
                if (err) {
                    res.status(401);
                    res.json({
                        message: 'Invalid Token 1'
                    });
                } else {
                    var username = decodedData.username;

                    authDao.checkAuth({
                        username: decodedData.username
                    })
                        .then(function (user) {
                            req.user = user;
                            next();
                        })
                        .catch(function (err) {
                            res.status(401);
                            res.json({
                                message: 'Invalid Token, User not found'
                            });
                        });
                }
            })
        } else {
            res.status(401);
            res.json({
                message: "Not Authorized"
            });
        }
    }
}